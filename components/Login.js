import React, { Component } from 'react'
import Router  from 'next/router';
import { connect } from 'react-redux';

class Login extends Component {
    constructor(props){
        super(props);
        this.props = props;
        this.state = {
            userName : '',
            password : ''
        }
    }
    login = () => {
        fetch('http://localhost:3000/user/login', {
            method: 'POST',
            credentials: 'same-origin',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                userName: this.state.userName,
                password: this.state.password
            })
        }).then(ans => {
            return ans.json()
        }).then(ans => {
            if(ans.resul != "password doesn't match"){
                this.props.register_data(ans.resul[0].idu,ans.resul[0].username,ans.resul[0].nombre,ans.resul[0].apellidos,ans.resul[0].fecha,ans.resul[0].linkfoto);
                Router.push('/profile');
            }
            else{
                Router.push('/');
            }
        })
    }
    
    render() {
        return (
            <div>
                <input title="Username" placeholder="User name" onChange = {(val) => {this.setState({userName : val.target.value})} }/>
                <input title="Password" placeholder="Password" onChange = {(val) => {this.setState({password : val.target.value})} }/>
                <button onClick = {() => {this.login()}}>Sign in</button>                
            </div>
        )
    }
}

let mapDispatchToProps = (dispatch) => {
    return{
        register_data : (idu,username,nombre,apellidos,fecha,linkfoto) => 
        {
            dispatch({type: 'SET_INITIAL_VALUES', idu, username, nombre,apellidos,fecha,linkfoto});
        }
    }
}

export default connect(null, mapDispatchToProps)(Login);