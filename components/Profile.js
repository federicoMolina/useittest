import React, { Component } from 'react'
import Router from 'next/router';
import {connect} from 'react-redux';
 class Profilex extends Component {
    constructor(props){
        super(props);
        this.props = props; 
    }
    componentDidMount(){
        if(this.props.user === null){
            Router.push('/')
        }
    }
    render() {
        return (
            <div>
                <div>
                    <img src={this.props.linkfoto}/>
                    <p>{this.props.username}</p>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        idu: state.idu,
        username: state.username,
        nombre: state.nombre,
        apellidos: state.apellidos,
        fecha: state.fecha,
        linkfoto: state.linkfoto
    }
}

export default connect (mapStateToProps)(Profilex)