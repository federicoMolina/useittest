import nextConnectRedux from 'next-connect-redux'
import {createStore} from 'redux'
import rootReducer from '../reducers'

export const store  = (initialState) => {return createStore(rootReducer, initialState)}

export const nextConnect = nextConnectRedux(store)