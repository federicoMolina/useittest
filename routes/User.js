const express = require('express');
const multer = require('multer');
let router = express.Router();
const bcrypt = require('bcryptjs');
const uuid = require('uuid');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const moment = require('moment');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './upload');
    },
    filename: function (req, file, cb) {
      cb(null,  Date.now()+ '-' +file.originalname );
    }
  })
const upload = multer({storage : storage});
const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres',
  host: 'database-2.cgvbvtwxwsbj.eu-west-1.rds.amazonaws.com',
  database: 'useitTest',
  password: 'Clave1234',
  port: 5432,
})

router.post('/',upload.single('ProfileImage') ,async (req, res, next) => {
    let errors = [];
    const {
        userName,
        password,
        password2,
        nombre,
        apellidos
    } = req.body 
    let id = uuid.v1();
    if (!userName || !password || !password2){
        errors.push({
            msg: "please fill in all fields"
        })
    }
    if(password !== password2){
        errors.push({
            msg: "Passwords do not match"
        })}
    if (errors.length > 0){
        res.send(errors)
    } else {
        console.log('a')
        pool.query('SELECT * FROM Usuario WHERE userName = $1', [userName], (error, results) => {
            if (error){
                res.send('error')
            }
            else{
                console.log(results.rows)
                if(results.rows.length > 0){
                    res.status(200).json(results.rows)
                }
                else{
                    bcrypt.genSalt(12, (err, salt) => {
                        bcrypt.hash(password, salt, (err, hash) => {
                            console.log(id);
                            console.log(moment().format());
                            console.log(req.file.path);
                            pool.query('INSERT INTO Usuario (idU,userName,nombre,apellidos,fecha,passwordx,linkfoto) VALUES ($1,$2,$3,$4,$5,$6,$7)', [id,userName,nombre,apellidos,moment().format(),hash,'./uploads/'+req.file.originalname],(error,results) => {
                                if(error){
                                    res.send(error);
                                }
                                else{
                                    res.json({ans : "created"});
                                }
                            })
                        })
                    })
                }
                
            }
        })
    }
    
})

router.post('/update', upload.single('ProfileImage'), (req, res, next) => {
    const {
        userName,
        password,
        password2,
        nombre,
        apellidos,
        id 
    } = req.body 
    if(!password){
        pool.query( 'UPDATE Usuario SET username=$1, nombre=$2, apellidos=$3, linkfoto=$4 WHERE idU=$5',
        [userName,nombre,apellidos,'./uploads/'+req.file.originalname,id],(error, result) => {
            if(error){
                
                res.send(error);
            }
            else{
                res.status(200).json({ans : result});
            }
        })
    }
    else{
        if(password !== password2){
            res.send("error")
        }
        else{
            bcrypt.genSalt(12, (err, salt) => {
                bcrypt.hash(password, salt, (err, hash) => { 
                    pool.query( 'UPDATE Usuario SET passwordx=$1 WHERE idU=$2',
            [hash,id],(error, result) => {
            if(error){
                
                res.send(error);
            }
            else{
                res.status(200).json({ans : result});
            }
        })
                })
            })
        }
    }
})


router.post('/login', (req, res, next) => {
    console.log(req.body.userName);
    pool.query("SELECT * from Usuario WHERE username=$1",[req.body.userName], (error, result) =>{
        if(error){
            res.status(501).send(error);
        }
        else{
            if(result.rows.length === 0){
                res.status(200).send("list empty");
            }
            else{
                console.log(result.rows);
                bcrypt.compare(req.body.password,result.rows[0].passwordx,(err, isMatch) => {
                    if(err){
                        res.status(501).send(err);
                    }
                    else{
                        if(isMatch){
                            res.json({resul : result.rows});
                        }
                        else{
                            res.json({resul : "password doesn't match"});
                        }
                    }
                });
            }
        }
    })

  })

module.exports = router;