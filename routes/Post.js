const express = require('express');
const multer = require('multer');
let router = express.Router();
const uuid = require('uuid');
const moment = require('moment');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './upload');
    },
    filename: function (req, file, cb) {
      cb(null,  Date.now()+ '-' +file.originalname );
    }
  });
const upload = multer({storage : storage});
const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres',
  host: 'database-2.cgvbvtwxwsbj.eu-west-1.rds.amazonaws.com',
  database: 'useitTest',
  password: 'Clave1234',
  port: 5432,
});

router.post('/', upload.single('PostImage'),(req, res, next) => {
    const {
        idU,
        comentario
    } = req.body
    let idP = uuid.v1();
    if(!comentario){
        comentario = "";
        pool.query("SELECT idP FROM post WHERE idP=$1",[idP], (error, result) => {
            if(error){
                res.send(error);
            }
            else{
                if(result.rows.length > 0){
                    res.send("exists");
                }
                else{
                    pool.query("INSERT INTO post (idP, idU, linkfoto, comentario, listamegusta,listacometarios) VALUES ($1,$2,$3,$4,$5,$6)",[idP,idU,req.file.path,comentario,{list : []},{list : []}],(error, result) => {
                        if(error){
                            res.send(error);
                        }
                        else{
                            res.json({ans : result});
                        }
                    });
                }
            }
        });
    }
    else{
        pool.query("SELECT idP FROM post WHERE idP=$1",[idP], (error, result) => {
            if(error){
                res.send(error);
            }
            else{
                if(result.rows.length > 0){
                    res.send("exists");
                }
                else{
                    pool.query("INSERT INTO post (idP, idU, linkfoto, comentario, listamegusta,listacometarios) VALUES ($1,$2,$3,$4,$5,$6)",[idP,idU,req.file.path,comentario,{list : []},{list : []}],(error, result) => {
                        if(error){
                            res.send(error);
                        }
                        else{
                            res.json({ans : result});
                        }
                    });
                }
            }
        });
    }
});

router.post('/update', upload.single('PostImage'), (req,res,next) => {
    const {
        idP,
        comentario,
        linkfoto,
        listamegusta,
        listacometarios
    } = req.body
    if(!linkfoto && !comentario){
        pool.query("UPDATE post SET listamegusta=$1, listacometarios=$2 WHERE idP=$3",
         [listamegusta, listacometarios,idP],
         (error, results)=>{
             if(error){
                 res.status(501).send(error)
             }
             else{
                 res.status(200).json({change : results});
             }
         });
    }
    else if(comentario){
        pool.query("UPDATE post SET cometario=$1 WHERE idP=$3",
         [comentario,idP],
         (error, results)=>{
             if(error){
                 res.status(501).send(error)
             }
             else{
                 res.status(200).json({change : results});
             }
         });
    }
    else{
        pool.query("UPDATE post SET linkfoto=$1 WHERE idP=$3",
        [req.file.path, idP], (error, results) => {
            if(error){
                res.status(501).send(error);
            }
            else{
                res.status(200).json({change : results});
            }
        })
    }

})
router.delete('/',(req,res,next)=> {
    const idP = req.body.idP;
    pool.query("DELETE FROM post WHERE idP=$1", [idP], (error, results) => {
        if(error){
            res.status(501).send(error);
        }
        else{
            res.status(200).json({change : results});
        }
    })
})

router.get('/', (req,res,next) => {
    pool.query("SELECT * from post", (error, results) => {
        if(error){
            res.status(500).send(error);
        }
        else{
            res.status(200).json({rows : results.rows});
        }
    })
})
module.exports = router;