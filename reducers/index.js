const initialState = {}

function rootReducer(state = initialState, action){
    if(action.type === 'SET_INITIAL_VALUES'){
        return{
            ...state, 
            idu : action.idu,
            username : action.username,
            nombre : action.nombre,
            apellidos : action.apellidos,
            fecha : action.fecha,
            linkfoto : action.linkfoto
        }
    }
    return state;
}

export default rootReducer;