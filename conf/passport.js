const passport = require('passport');
const localStrategy = require('passport-local').Strategy
//const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres',
  host: 'database-2.cgvbvtwxwsbj.eu-west-1.rds.amazonaws.com',
  database: 'useitTest',
  password: 'Clave1234',
  port: 5432,
  });
//load user model
  passport.use(
    new localStrategy((userName, password, done) => {
      console.log("something");
      pool.query("SELECT * from User WHERE userName=$1",[userName], (error, result) =>{
        if(error){
          return done(null, false, {message: 'error'});
        }
        else{
          if(result.rows.length === 0){
            return done(null, false, {message: 'that user is not registered'});
          }
          else{
            console.log(result.rows);
            let passwordx = result.rows[0].passwordx
            bcrypt.compare(password, passwordx, (err, isMatch) => {
              if(err){
                console.log('error');
              };
              if(isMatch){
                return done(null, result.rows[0]);
              }
              else{
                return done(null, false, {message: 'password incorrect'})
              }
            })
          }
        }
      })      
    })
  )
  /*passport.use(new JwtStrategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey   : 'your_jwt_secret'
    },
    function (jwtPayload, done) {

      Account.findOne({id: jwtPayload.sub}, function(err, Account) {
        if (err) {
            return done(err, false);
        }
        if (Account) {
            return done(null, Account);
        } else {
            return done(null, false);
            // or you could create a new account
        }
    });
    }
));*/

  passport.serializeUser((Account, done) => {
      done(null, Account.id);
  });
  passport.deserializeUser((id, done) => {
    Account.findById(id, (err, Account) => {
      done(err, Account)
    })})
module.exports = passport
