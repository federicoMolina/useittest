let passport = require('passport')
let session = require('express-session')
let express = require('express')
require('dotenv').config()
const dev = process.env.NODE_DEV !== 'production' //true false
let next = require('next')
const nextApp = next({ dev })
const handle = nextApp.getRequestHandler();
const PORT = process.env.PORT || 3000
let bodyParser = require('body-parser')
const User = require('./routes/User');
require('./conf/passport');
nextApp.prepare().then(() => {
    const app = express()
    app.use(bodyParser.json())
    app.use(session({
        secret: 's3cr3t',
        reave: true,
        saveUninitialized: true 
    }))
    app.use(passport.initialize())
    app.use(passport.session())
    app.use('/user',User);
    app.get('*', (req,res) => {
        return handle(req,res) // for all the react stuff
    })
    app.listen(PORT, err => {
        if (err) throw err;
        console.log(`ready at http://localhost:${PORT}`)
    })
}).catch(ex => {
    console.error(ex.stack)
    process.exit(1)
})